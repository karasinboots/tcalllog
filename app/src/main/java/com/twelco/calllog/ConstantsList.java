package com.twelco.calllog;

import com.twelco.calllog.service.CallService;

/**
 * Created by karasinboots on 18.12.2017.
 */

public class ConstantsList {
    public final static String INTERNET_BROADCAST = "internet_method";
    public final static String TRACK_IN = "incoming";
    public final static String TRACK_OUT = "outgoing";
    public final static String DEF_IN = "default_in";
    public final static String DEF_OUT = "default_out";
    public final static String PREFERENCES = "network";
    public final static String ALLOW_GPRS = "allowGprs";
    public final static String CONFIRM_OUT = "confirmation_out";
    public final static String CONFIRM_IN = "confirmation_in";
    public final static String UPLOAD_BROADCAST = "upload";
    public final static String TIMEOUT = "timeout";
    public static final int HANGED = 1;
    public static final int REJECTED_CALL = 0;
    public static final String STOP_BROADCAST = CallService.class.getName() + "stop";
    public static final String ERROR_BROADCAST = CallService.class.getName() + "error";
    public static final String NOTIFY_ACCEPT = "notify_accepted";
    public static final String NOTIFY_DECLINE = "notify_declined";
    public static final String CHANNEL_ID = "my_channel_01";
    public static final String SYNCED_BROADCAST = "syncedbroadcast";
    public static final String QUERY_BROADCAST = CallService.class.getName() + "query";
}
