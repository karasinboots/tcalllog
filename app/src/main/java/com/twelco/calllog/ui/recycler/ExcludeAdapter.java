package com.twelco.calllog.ui.recycler;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.twelco.calllog.R;
import com.twelco.calllog.database.AppDatabase;

import java.util.List;


public class ExcludeAdapter extends RecyclerView.Adapter<ExcludeViewHolder> {
    private final List<Exclude> excludeList;
    private Context context;

    public ExcludeAdapter(List<Exclude> excludeList) {
        this.excludeList = excludeList;
    }

    @Override
    public ExcludeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.exclude_item, parent, false);
        return new ExcludeViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ExcludeViewHolder holder, int position) {
        changeSize();
        //getting data for each item
        Exclude item = excludeList.get(position);
        //set tags to view elements to get they position in future
        holder.excludedName.setTag(position);
        holder.delete.setTag(position);
        holder.add.setTag(position);
        holder.enabled.setTag(position);
        //if item in exclude list contains checked element, check it in interface
        if (excludeList.get(position).isChecked() == 1)
            holder.enabled.setChecked(true);
        else holder.enabled.setChecked(false);
        //set text to EditText
        holder.excludedName.setText(item.getNumber());
        //set onClickListener to add button of RecyclerView item to add new line in exclude list
        holder.add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if previous element of exclude list contains at least 3 symbols, add new
                if (excludeList.get(excludeList.size() - 1).getNumber().length() > 3) {
                    putExcludeToDb(excludeList);
                    excludeList.add(new Exclude("", 0));
                    notifyItemInserted(excludeList.size());
                } else holder.excludedName.setError("From 3 to 15 symbols!");
            }
        });
        //set onClickListener to delete button of RecyclerView item to add new line in exclude list
        holder.delete.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    //if exclude list contains more than one element, delete last
                    if (excludeList.size() > 1) {
                        excludeList.remove(holder.getAdapterPosition());
                        putExcludeToDb(excludeList);
                        notifyItemRemoved(holder.getAdapterPosition());
                    } else {
                        //if contain only one - replace it by empty element
                        excludeList.set(holder.getAdapterPosition(), new Exclude("", 0));
                        putExcludeToDb(excludeList);
                        notifyDataSetChanged();
                    }
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
            }
        });
        //if checkBox in item is checked, put this action to database, and from now this element was enabled
        holder.enabled.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (holder.excludedName.getText().length() > 3 && holder.excludedName.getText().length() < 15) {
                    excludeList.set((Integer) holder.excludedName.getTag(),
                            new Exclude(holder.excludedName.getText().toString(), holder.enabled.isChecked() ? 1 : 0));
                    putExcludeToDb(excludeList);
                } else {
                    holder.excludedName.setError("From 3 to 15 symbols!");
                    holder.enabled.setChecked(false);
                }

            }
        });
        //add to edittext in recyclerview item text change listener
        holder.excludedName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            //when text changed put exclude item data to database and put to list for recyclerview
            @Override
            public void afterTextChanged(Editable s) {
                Exclude exclude = new Exclude(s.toString(), holder.enabled.isChecked() ? 1 : 0);
                try {
                    excludeList.set((Integer) holder.excludedName.getTag(),
                            exclude);
                    putExcludeToDb(excludeList);
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void putShared(int size) {
        SharedPreferences sPref = context.getSharedPreferences("network", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sPref.edit();
        editor.putInt("size", size);
        editor.apply();
    }

    private void changeSize() {
        int checked = 0;
        Log.e("checked", String.valueOf(checked));
        //to get count of numbers needed to exclude get all from db and check counter of checked
        List<com.twelco.calllog.database.exclude.Exclude> list = AppDatabase.getInstance(context).excludeDao().getAll();
        Log.e("listsize", String.valueOf(list.size()));
        for (com.twelco.calllog.database.exclude.Exclude exclude : list) {
            if (exclude.getChecked() == 1) {
                checked++;
                Log.e("checked", String.valueOf(checked));
            }
        }
        //put checked counter to sharedpreferences to get it in main fragment and update counter
        putShared(checked);
    }

    //operations with database need to be started in new thread
    private void putExcludeToDb(List<Exclude> excludeList) {
        @SuppressLint("StaticFieldLeak")
        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected Void doInBackground(Object[] objects) {
                //clear exclude list before adding elements
                AppDatabase.getInstance(context).excludeDao().clearExclude();
                List<Exclude> excludeList = (List<Exclude>) objects[0];
                for (int i = 0; i < excludeList.size(); i++) {
                    Exclude exclude = excludeList.get(i);
                    AppDatabase.getInstance(context).excludeDao().insertOne(new com.twelco.calllog.database.exclude.Exclude(exclude.getNumber(), exclude.isChecked()));
                }
                changeSize();
                return null;
            }
        };
        asyncTask.execute(excludeList);
    }


    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return excludeList.size();
    }
}
