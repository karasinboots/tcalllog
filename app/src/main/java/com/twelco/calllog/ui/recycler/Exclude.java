package com.twelco.calllog.ui.recycler;

import java.util.ArrayList;
import java.util.List;

//data for exclude list recycler filling
public class Exclude {
    private String number;
    private int checked;
    private List<Exclude> excludeList = new ArrayList<>();

    public Exclude(String name, int checked) {
        this.number = name;
        this.checked = checked;
    }

    public Exclude() {
    }

    public String getNumber() {
        return number;
    }

    public List<Exclude> getExclude() {
        return excludeList;
    }

    public int isChecked() {
        return checked;
    }

    public void addExcluded(String excludedName, int checked) {
        excludeList.add(new Exclude(excludedName, checked));
    }

}
