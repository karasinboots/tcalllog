package com.twelco.calllog.ui;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.twelco.calllog.R;
import com.twelco.calllog.database.AppDatabase;
import com.twelco.calllog.ui.recycler.Exclude;
import com.twelco.calllog.ui.recycler.ExcludeAdapter;

import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by karasinboots on 15.12.2017.
 */

public class ExclusionFragment extends Fragment {
    @BindView(R.id.exclude_recycler)
    RecyclerView excludeRecycler;
    Exclude exclude;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.exclude_fragment, container, false);
        ButterKnife.bind(this, v);
        try {
            //get exclude list from database
            getExcludeFromDb();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //creating excluded adapter for excluded RecyclerView
        ExcludeAdapter excludeAdapter = new ExcludeAdapter(exclude.getExclude());
        excludeAdapter.setContext(getContext());
        //creating standard linearlayoutmanager for excluded recyclerview
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        excludeRecycler.setLayoutManager(linearLayoutManager);
        //linking adapter to recyclerview
        excludeRecycler.setAdapter(excludeAdapter);
    }

    @SuppressLint("StaticFieldLeak")
    private void getExcludeFromDb() throws ExecutionException, InterruptedException {
        //get list of exclude numbers from database
        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                Exclude exclude = new Exclude();
                //adding to list of excluded all from database
                for (int i = 0; i < AppDatabase.getInstance(getContext()).excludeDao().getAll().size(); i++) {
                    exclude.addExcluded(AppDatabase.getInstance(getContext()).excludeDao().getAll().get(i).getExcludeNumber(),
                            AppDatabase.getInstance(getContext()).excludeDao().getAll().get(i).getChecked());
                }
                //if list of excluded empty, add one with empty fields
                if (exclude.getExclude().size() == 0)
                    exclude.addExcluded("", 0);
                return exclude;
            }
        };
        exclude = (Exclude) asyncTask.execute().get();
    }
}
