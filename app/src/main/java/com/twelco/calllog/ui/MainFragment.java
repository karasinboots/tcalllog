package com.twelco.calllog.ui;

import android.Manifest;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.twelco.calllog.BuildConfig;
import com.twelco.calllog.MainActivity;
import com.twelco.calllog.R;
import com.twelco.calllog.database.AppDatabase;
import com.twelco.calllog.database.exclude.Exclude;
import com.twelco.calllog.service.CallService;
import com.twelco.calllog.service.NetworkManager;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

import static android.content.Context.MODE_PRIVATE;
import static android.content.Context.NOTIFICATION_SERVICE;
import static com.twelco.calllog.ConstantsList.ALLOW_GPRS;
import static com.twelco.calllog.ConstantsList.CHANNEL_ID;
import static com.twelco.calllog.ConstantsList.CONFIRM_IN;
import static com.twelco.calllog.ConstantsList.CONFIRM_OUT;
import static com.twelco.calllog.ConstantsList.DEF_IN;
import static com.twelco.calllog.ConstantsList.DEF_OUT;
import static com.twelco.calllog.ConstantsList.ERROR_BROADCAST;
import static com.twelco.calllog.ConstantsList.INTERNET_BROADCAST;
import static com.twelco.calllog.ConstantsList.PREFERENCES;
import static com.twelco.calllog.ConstantsList.STOP_BROADCAST;
import static com.twelco.calllog.ConstantsList.SYNCED_BROADCAST;
import static com.twelco.calllog.ConstantsList.TIMEOUT;
import static com.twelco.calllog.ConstantsList.TRACK_IN;
import static com.twelco.calllog.ConstantsList.TRACK_OUT;

/**
 * Created by karasinboots on 15.12.2017.
 */

public class MainFragment extends Fragment {
    @BindView(R.id.button_start)
    Button buttonStart;
    @BindView(R.id.button_stop)
    Button buttonStop;
    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;
    @BindView(R.id.box_wifi)
    RadioButton boxWifiOnly;
    @BindView(R.id.box_gprs)
    RadioButton boxGprs;
    @BindView(R.id.editText)
    EditText uidText;
    @BindView(R.id.number)
    EditText number;
    @BindView(R.id.timeout)
    EditText timeout;
    @BindView(R.id.textView5)
    TextView version;
    @BindView(R.id.chkIncoming)
    CheckBox chkIncoming;
    @BindView(R.id.chkOutgoing)
    CheckBox chkOutgoing;
    @BindView(R.id.outDefOff)
    RadioButton outDefNo;
    @BindView(R.id.outDefYes)
    RadioButton outDefYes;
    @BindView(R.id.incDefNo)
    RadioButton incDefNo;
    @BindView(R.id.incDefYes)
    RadioButton incDefYes;
    @BindView(R.id.rgIn)
    RadioGroup rgIn;
    @BindView(R.id.rgOut)
    RadioGroup rgOut;
    @BindView(R.id.incomingConfirmChk)
    CheckBox incomingConfirmChk;
    @BindView(R.id.outgoingConfirmChk)
    CheckBox outgoingConfirmChk;
    @BindView(R.id.manageExclusions)
    Button manageExclusions;
    @BindView(R.id.exclusions)
    TextView exclusions;
    @BindView(R.id.lastSync)
    TextView lastSync;
    Context mContext;

    Intent mServiceIntent;
    SharedPreferences sPref;
    BroadcastReceiver stopReceiver;
    BroadcastReceiver errorReceiver;
    BroadcastReceiver lastSyncReceiver;
    boolean error = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.main_fragment, container, false);
        //registering stop receiver to enable/disable buttons when service stops
        stopReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                buttonStart.setEnabled(true);
                buttonStop.setEnabled(false);
            }
        };
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(stopReceiver, new IntentFilter(STOP_BROADCAST));
        //registering error receiver to update error message in info field
        errorReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                lastSync.setText("Info: " + intent.getStringExtra("message"));
                error = true;
            }
        };
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(errorReceiver, new IntentFilter(ERROR_BROADCAST));
        //registering lastSync receiver to update field of last success synced
        lastSyncReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                error = false;
                getLastSync();
            }
        };
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(lastSyncReceiver, new IntentFilter(SYNCED_BROADCAST));
        return v;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        mContext = getContext();

        //all about interface, setting buttons, radiobuttons, edittexts and more and more

        version.setText(BuildConfig.VERSION_NAME);
        sPref = mContext.getSharedPreferences(PREFERENCES, MODE_PRIVATE);
        lastSync.setText("Info: " + sPref.getString("errorMessage", ""));
        //fill field with excluded number
        changeExcludedSize();
        exclusions.setText(String.valueOf(sPref.getInt("size", 0)));
        //update last sync time every minute
        setupSyncTimeUpdate();
        //request nedded to app permissions like access to phone state and outgoing calls
        requestPermission();
        mServiceIntent = new Intent(mContext, CallService.class);
        if (isMyServiceRunning(CallService.class)) {
            buttonStart.setEnabled(false);
        }
        if (!isMyServiceRunning(CallService.class)) {
            buttonStop.setEnabled(false);
        }
        buttonStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startService();
            }
        });
        buttonStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isMyServiceRunning(CallService.class)) {
                    mContext.stopService(mServiceIntent);
                    buttonStart.setEnabled(true);
                    buttonStop.setEnabled(false);
                    changeNotification();
                }
            }
        });
        manageExclusions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.support.v4.app.FragmentTransaction transaction = getFragmentManager().beginTransaction();
                ExclusionFragment exclusionFragment = new ExclusionFragment();
                transaction.replace(R.id.frame, exclusionFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
        if (NetworkManager.isGprsAllowed(mContext))
            boxGprs.setChecked(true);
        else boxWifiOnly.setChecked(true);
        radioGroup.setOnCheckedChangeListener(
                new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        switch (checkedId) {
                            case R.id.box_gprs:
                                putShared(ALLOW_GPRS, true);
                                sendBroadcastMessage(INTERNET_BROADCAST);
                                Toast.makeText(mContext, "Using mobile networks allowed!", Toast.LENGTH_SHORT).show();
                                break;
                            case R.id.box_wifi:
                                putShared(ALLOW_GPRS, false);
                                Toast.makeText(mContext, "Use only WiFi to sync!", Toast.LENGTH_SHORT).show();
                                break;
                        }
                    }
                }
        );
        outgoingConfirmChk.setChecked(sPref.getBoolean(CONFIRM_OUT, false));
        incomingConfirmChk.setChecked(sPref.getBoolean(CONFIRM_IN, false));
        outgoingConfirmChk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                putShared(CONFIRM_OUT, isChecked);
                if (isChecked) {
                    Toast.makeText(mContext, "Prompt to write outgoing.", Toast.LENGTH_SHORT).show();
                    rgOut.setEnabled(true);
                    outDefYes.setEnabled(true);
                    outDefNo.setEnabled(true);
                } else {
                    Toast.makeText(mContext, "Do not prompt to write outgoing", Toast.LENGTH_SHORT).show();
                    rgOut.setEnabled(false);
                    outDefYes.setEnabled(false);
                    outDefNo.setEnabled(false);
                }
            }
        });
        incomingConfirmChk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                putShared(CONFIRM_IN, isChecked);
                if (isChecked) {
                    rgIn.setEnabled(true);
                    incDefYes.setEnabled(true);
                    incDefNo.setEnabled(true);
                    Toast.makeText(mContext, "Prompt to write incoming.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mContext, "Do not prompt to write incoming.", Toast.LENGTH_SHORT).show();
                    rgIn.setEnabled(false);
                    incDefYes.setEnabled(false);
                    incDefNo.setEnabled(false);
                }
            }
        });
        chkOutgoing.setChecked(sPref.getBoolean(TRACK_OUT, true));
        chkOutgoing.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                putShared(TRACK_OUT, isChecked);
                checkOutButtons();
                if (isChecked) {
                    outgoingConfirmChk.setEnabled(true);
                    Toast.makeText(mContext, "Track outgoing.", Toast.LENGTH_SHORT).show();
                } else {
                    outgoingConfirmChk.setEnabled(false);
                    rgOut.setEnabled(false);
                    outDefYes.setEnabled(false);
                    outDefNo.setEnabled(false);
                    Toast.makeText(mContext, "Do not track outgoing", Toast.LENGTH_SHORT).show();
                }
            }
        });
        chkIncoming.setChecked(sPref.getBoolean(TRACK_IN, true));
        chkIncoming.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                putShared(TRACK_IN, isChecked);
                checkInButtons();
                if (isChecked) {
                    incomingConfirmChk.setEnabled(true);
                    Toast.makeText(mContext, "Track incoming.", Toast.LENGTH_SHORT).show();
                } else {
                    rgIn.setEnabled(false);
                    incDefYes.setEnabled(false);
                    incDefNo.setEnabled(false);
                    Toast.makeText(mContext, "Do not track incoming", Toast.LENGTH_SHORT).show();
                    incomingConfirmChk.setEnabled(false);
                }
            }
        });
        incomingConfirmChk.setEnabled(chkIncoming.isChecked());
        outgoingConfirmChk.setEnabled(chkOutgoing.isChecked());
        uidText.setText(sPref.getString("uid", ""));
        uidText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                putUid(s.toString());
                if (s.length() < 3 || s.length() > 15) uidText.setError("From 3 to 15 symbols!");
            }
        });
        number.setText(sPref.getString("number", ""));
        number.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() < 15)
                    putNumber(s.toString());
                else number.setError("15 symbols is maximum length!");
            }
        });
        timeout.setText(String.valueOf(sPref.getInt(TIMEOUT, 5000) / 1000));
        timeout.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 0) {
                    if (Integer.parseInt(s.toString()) < 5) {
                        putTimeout(5000);
                        timeout.setError("From 5 to 90 seconds");
                    } else if (Integer.parseInt(s.toString()) > 90) {
                        putTimeout(90000);
                        timeout.setText("90");
                        timeout.setError("From 5 to 90 seconds");
                    } else putTimeout(Integer.parseInt(s.toString()) * 1000);
                }
            }
        });
        if (sPref.getBoolean(DEF_OUT, true))
            outDefYes.setChecked(true);
        else outDefNo.setChecked(true);
        if (chkOutgoing.isChecked() && outgoingConfirmChk.isChecked()) {
            rgOut.setEnabled(true);
            outDefYes.setEnabled(true);
            outDefNo.setEnabled(true);
        } else {
            rgOut.setEnabled(false);
            outDefYes.setEnabled(false);
            outDefNo.setEnabled(false);
        }
        rgOut.setOnCheckedChangeListener(
                new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        switch (checkedId) {
                            case R.id.outDefYes:
                                putShared(DEF_OUT, true);
                                Toast.makeText(mContext, "Write outgoing by default.", Toast.LENGTH_SHORT).show();
                                break;
                            case R.id.outDefOff:
                                putShared(DEF_OUT, false);
                                Toast.makeText(mContext, "Ignore outgoing by default.", Toast.LENGTH_SHORT).show();
                                break;
                        }
                    }
                }
        );
        if (sPref.getBoolean(DEF_IN, true))
            incDefYes.setChecked(true);
        else incDefNo.setChecked(true);
        if (chkIncoming.isChecked() && incomingConfirmChk.isChecked()) {
            rgIn.setEnabled(true);
            incDefYes.setEnabled(true);
            incDefNo.setEnabled(true);
        } else {
            rgIn.setEnabled(false);
            incDefYes.setEnabled(false);
            incDefNo.setEnabled(false);
        }
        rgIn.setOnCheckedChangeListener(
                new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        switch (checkedId) {
                            case R.id.incDefYes:
                                putShared(DEF_IN, true);
                                Toast.makeText(mContext, "Write incoming by default.", Toast.LENGTH_SHORT).show();
                                break;
                            case R.id.incDefNo:
                                putShared(DEF_IN, false);
                                Toast.makeText(mContext, "Ignore incoming by default.", Toast.LENGTH_SHORT).show();
                                break;
                        }
                    }
                }
        );
    }

    private void setupSyncTimeUpdate() {
        final Handler handler = new Handler(Looper.getMainLooper());
        Runnable runnable = new Runnable() {
            @Override
            public synchronized void run() {
                if (!error)
                    getLastSync();
                handler.postDelayed(this, 60000);
            }
        };
        handler.postDelayed(runnable, 60000);
    }

    private void checkOutButtons() {
        outDefYes.setEnabled(outgoingConfirmChk.isChecked());
        outDefNo.setEnabled(outgoingConfirmChk.isChecked());
    }

    private void checkInButtons() {
        incDefYes.setEnabled(incomingConfirmChk.isChecked());
        incDefNo.setEnabled(incomingConfirmChk.isChecked());
    }

    private void getLastSync() {
        String lastSyncTime = convertDate(System.currentTimeMillis() -
                sPref.getLong("synctime", System.currentTimeMillis())) + " ago";
        lastSync.setText("Last sync: " + lastSyncTime);
        putMessage("Last sync: " + lastSyncTime);
    }

    private void putMessage(String message) {
        SharedPreferences.Editor editor = sPref.edit();
        editor.putString("errorMessage", message);
        editor.apply();
    }

    public static String convertDate(long dateInMilliseconds) {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        return sdf.format(dateInMilliseconds);
    }

    private void putShared(int size) {
        SharedPreferences sPref = mContext.getSharedPreferences("network", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sPref.edit();
        editor.putInt("size", size);
        editor.apply();
    }

    private void changeExcludedSize() {
        int checked = 0;
        Log.e("checked", String.valueOf(checked));
        List<Exclude> list = AppDatabase.getInstance(mContext).excludeDao().getAll();
        Log.e("listsize", String.valueOf(list.size()));
        for (com.twelco.calllog.database.exclude.Exclude exclude : list) {
            if (exclude.getChecked() == 1) {
                checked++;
                Log.e("checked", String.valueOf(checked));
            }
        }
        putShared(checked);
    }

    private void sendBroadcastMessage(String filter) {
        Intent intent = new Intent(filter);
        Log.e("broadcast_send ", "sended");
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
    }

    private void changeNotification() {

        Intent notificationIntent = new Intent(mContext, MainActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            CharSequence name = "TCallLog";
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            Notification.Builder builder = new Notification.Builder(mContext, CHANNEL_ID)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText("Service not running")
                    .setChannelId(CHANNEL_ID)
                    .setContentIntent(pendingIntent)
                    .setOngoing(true)
                    .setAutoCancel(false)
                    .setSmallIcon(R.drawable.ic_inactive);
            Notification notification = builder.build();
            NotificationManager notificationManager =
                    (NotificationManager) mContext.getSystemService(NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(mChannel);
            notificationManager.notify(1000, notification);
        } else {
            NotificationCompat.Builder builder = new NotificationCompat.Builder(mContext, CHANNEL_ID)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText("Service not running")
                    .setContentIntent(pendingIntent)
                    .setOngoing(true)
                    .setAutoCancel(false)
                    .setSmallIcon(R.drawable.ic_inactive).setChannelId(CHANNEL_ID)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);
            Notification notification = builder.build();
            NotificationManagerCompat notificationManager =
                    NotificationManagerCompat.from(mContext);
            notificationManager.notify(1000, notification);
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            Log.i("isMyServiceRunning?", service.service.getClassName()+"0");
            if ((serviceClass.getName()).equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }

    private void putUid(String uid) {
        SharedPreferences.Editor editor = sPref.edit();
        editor.putString("uid", uid);
        Log.d("uid", uid);
        editor.apply();
    }

    private void putNumber(String number) {
        SharedPreferences.Editor editor = sPref.edit();
        editor.putString("number", number);
        editor.apply();
    }

    private void putShared(String method, boolean status) {
        SharedPreferences.Editor editor = sPref.edit();
        editor.putBoolean(method, status);
        editor.apply();
    }

    private void putTimeout(int time) {
        SharedPreferences.Editor editor = sPref.edit();
        editor.putInt(TIMEOUT, time);
        editor.apply();
    }

    @AfterPermissionGranted(1)
    private void requestPermission() {
        String[] perms = {Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.PROCESS_OUTGOING_CALLS,Manifest.permission.ACCESS_NETWORK_STATE};
        if (!EasyPermissions.hasPermissions(mContext, perms)) {
            // Do not have permissions, request them now
            EasyPermissions.requestPermissions(this, "To make it work",
                    1, perms);
        }
    }

    private void startService() {
        //check if service is already running
        if (!isMyServiceRunning(CallService.class)) {
            //check number and uid fields
            if (sPref.getString("number", "").length() > 0
                    && sPref.getString("number", "").length() < 15) {
                if (uidText.getText().length() != 0) {
                    //check incoming and outgoing racking
                    if (chkIncoming.isChecked() || chkOutgoing.isChecked()) {
                        //if alright, starting service
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            mContext.startForegroundService(new Intent(mContext, CallService.class));
                            buttonStart.setEnabled(false);
                        } else {
                            mContext.startService(new Intent(mContext, CallService.class));
                            buttonStart.setEnabled(false);
                        }
                        buttonStop.setEnabled(true);
                    } else
                        //if not - toast or error in edittext
                        Toast.makeText(mContext, "Check type of calls to track!", Toast.LENGTH_SHORT).show();
                } else uidText.setError("Fill!");
            } else number.setError("Fill!");
        }
    }


}
