package com.twelco.calllog.ui.recycler;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;

import com.twelco.calllog.R;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ExcludeViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.excluded_name)
    EditText excludedName;
    @BindView(R.id.enabled)
    CheckBox enabled;
    @BindView(R.id.imageView)
    ImageView delete;
    @BindView(R.id.imageView2)
    ImageView add;

    public ExcludeViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
