package com.twelco.calllog;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by karasinboots on 23.12.2017.
 */

public class TimerUtil {
    Timer timer;
    TimerTask timerTask;
    int number;
    List<TimerUtil> list = new ArrayList<>();

    public TimerUtil() {
    }

    public int getNumber() {
        return number;
    }

    public Timer getTimer() {
        return timer;
    }

    public TimerUtil(Timer timer, TimerTask timerTask, int number) {
        this.timer = timer;
        this.number = number;
        this.timerTask = timerTask;
    }

    public TimerTask getTimerTask() {
        return timerTask;
    }

    public List<TimerUtil> getList() {
        return list;
    }

    public void addTimer(TimerUtil timerUtil) {
        list.add(timerUtil);
    }
}
