package com.twelco.calllog;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.twelco.calllog.database.AppDatabase;
import com.twelco.calllog.service.CallService;
import com.twelco.calllog.ui.ExclusionFragment;
import com.twelco.calllog.ui.MainFragment;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.ButterKnife;
import io.fabric.sdk.android.Fabric;

import static com.twelco.calllog.ConstantsList.UPLOAD_BROADCAST;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Context mContext;
    Timer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);
        mContext = getApplicationContext();
        ButterKnife.bind(this);
        //registering receiver for changing counters if query changed
        registerReciever();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        MainFragment mainFragment = new MainFragment();
        android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame, mainFragment);
        transaction.commit();
        //registering timer that check query every minute. to prevent battery leak we unregster
        // it when activity paused
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                fillHour();
            }
        }, 0, 60000);
        //get query for the first time when activity started
        getQuery();
    }

    private void registerReciever() {
        String QUERY_BROADCAST = CallService.class.getName() + "query";
        LocalBroadcastManager.getInstance(mContext).registerReceiver(
                new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        //get query and fill hour and day counter
                        getQuery();
                        fillHour();
                    }
                }, new IntentFilter(QUERY_BROADCAST)
        );
    }

    private void fillHour() {
        @SuppressLint("StaticFieldLeak")
        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected int[] doInBackground(Object[] objects) {
                //get time in millis to last hour from this moment
                long hour = System.currentTimeMillis() - 3600000;
                //get time in millis fom 00:00
                Calendar now = Calendar.getInstance();
                now.set(Calendar.HOUR_OF_DAY, 0);
                now.set(Calendar.MINUTE, 0);
                now.set(Calendar.SECOND, 0);
                //getting synced by last hour and day
                int byHour = AppDatabase.getInstance(mContext).userDao().getByHour(hour).size();
                int byDay = AppDatabase.getInstance(mContext).userDao().getByHour(now.getTimeInMillis()).size();
                //if day is over, clean old data from database
                AppDatabase.getInstance(mContext).userDao().cleanOld(now.getTimeInMillis());
                return new int[]{byDay, byHour};
            }

            @Override
            protected void onPostExecute(Object o) {
                int[] data = (int[]) o;
                //fill counters field with data
                NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
                View headerView = navigationView.getHeaderView(0);
                TextView byHour = (TextView) headerView.findViewById(R.id.byHour);
                byHour.setText(String.valueOf(data[1]));
                TextView byDay = (TextView) headerView.findViewById(R.id.byDay);
                byDay.setText(String.valueOf(data[0]));
                invalidateOptionsMenu();
            }
        };
        asyncTask.execute();
    }

    private void getQuery() {
        @SuppressLint("StaticFieldLeak")
        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected Integer doInBackground(Object[] objects) {
                //get unsynced from database
                return AppDatabase.getInstance(mContext).userDao().getUnsynced().size();
            }

            @Override
            protected void onPostExecute(Object o) {
                //fill counters and make button visible if query > 0
                NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
                View headerView = navigationView.getHeaderView(0);
                TextView excSize = (TextView) headerView.findViewById(R.id.excSize);
                excSize.setText(String.valueOf((Integer) o));
                invalidateOptionsMenu();
            }
        };
        asyncTask.execute();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        Log.e("unsynced size", AppDatabase.getInstance(mContext).userDao().getUnsynced().size() + "");
        if (AppDatabase.getInstance(mContext).userDao().getUnsynced().size() > 0)
            menu.setGroupVisible(R.id.groupVsbl, true);
        else menu.setGroupVisible(R.id.groupVsbl, false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.item2) {
            sendBroadcastMessage(UPLOAD_BROADCAST);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        android.support.v4.app.FragmentTransaction transaction;
        int id = item.getItemId();
        switch (id) {
            case R.id.sync:
                item.setCheckable(false);
                sendBroadcastMessage(UPLOAD_BROADCAST);
                break;
            case R.id.exclusions:
                ExclusionFragment exclusionFragment = new ExclusionFragment();
                transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame, exclusionFragment);
                transaction.addToBackStack(null);
                transaction.commit();
                break;
            case R.id.about:
                item.setCheckable(false);
                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                alertDialog.setTitle("About");
                alertDialog.setMessage(" TCallLog is incoming and outgoing call collecting software.");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
                break;
            case R.id.web:
                item.setCheckable(false);
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twelco.com/intro"));
                startActivity(browserIntent);
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void sendBroadcastMessage(String filter) {
        //send upload broadcast when we pressing sync buttons
        Intent intent = new Intent(filter);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }

    @Override
    public void onStop() {
        timer.cancel();
        super.onStop();
    }

    @Override
    public void onPause() {
        timer.cancel();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
