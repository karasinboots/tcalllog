package com.twelco.calllog.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.twelco.calllog.database.exclude.Exclude;
import com.twelco.calllog.database.exclude.ExcludeDao;
import com.twelco.calllog.database.user.User;
import com.twelco.calllog.database.user.UserDao;
//description of database, what entities we use, what DAOs we use
@Database(entities = {User.class, Exclude.class}, version = 6)
public abstract class AppDatabase extends RoomDatabase {
    public abstract UserDao userDao();

    public abstract ExcludeDao excludeDao();

    public static AppDatabase instance = null;
    //instantiating of database is heavy, so we using static instance
    public static AppDatabase getInstance(Context context) {
        if (instance == null) {
            instance = create(context);
        }
        return instance;
    }
    //creating database
    private static AppDatabase create(final Context context) {
        //build database with permission to clean previous version after updating database(not app)
        //and make permission to access to database in main thread(we didn't use it in hard cases, so we can)
        return Room.databaseBuilder(
                context,
                AppDatabase.class,
                "call-database").fallbackToDestructiveMigration().
                allowMainThreadQueries().build();
    }
}