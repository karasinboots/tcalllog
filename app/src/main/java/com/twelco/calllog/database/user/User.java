package com.twelco.calllog.database.user;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;

//entity of user calls
@Entity
public class User implements Serializable {
    @PrimaryKey(autoGenerate = true)
    private int uid;

    @ColumnInfo(name = "call_number")
    private String callNumber;
    @ColumnInfo(name = "call_time")
    private long callTime;
    @ColumnInfo(name = "call_length")
    private long callLength;
    @ColumnInfo(name = "hanged")
    private int hanged;
    @ColumnInfo(name = "isOutgoing")
    private int isOutgoing;
    @ColumnInfo(name = "synced")
    private int synced;

    public User(String callNumber, long callTime, long callLength, int hanged, int isOutgoing, int synced) {
        this.callNumber = callNumber;
        this.callTime = callTime;
        this.callLength = callLength;
        this.hanged = hanged;
        this.isOutgoing = isOutgoing;
        this.synced = synced;
    }

    public int getSynced() {
        return synced;
    }

    public void setSynced(int synced) {
        this.synced = synced;
    }

    public long getCallTime() {
        return callTime;
    }

    public void setCallTime(long callTime) {
        this.callTime = callTime;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getIsOutgoing() {
        return isOutgoing;
    }

    public void setIsOutgoing(int isOutgoing) {
        this.isOutgoing = isOutgoing;
    }

    public String getCallNumber() {
        return callNumber;
    }

    public void setCallNumber(String callNumber) {
        this.callNumber = callNumber;
    }

    public long getCallLength() {
        return callLength;
    }

    public void setCallLength(long callLength) {
        this.callLength = callLength;
    }

    public int getHanged() {
        return hanged;
    }

    public void setHanged(int hanged) {
        this.hanged = hanged;
    }

}
