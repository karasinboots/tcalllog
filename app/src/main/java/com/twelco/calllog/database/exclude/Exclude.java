package com.twelco.calllog.database.exclude;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

//entity of exclude list numbers
@Entity
public class Exclude {
    //unique auto generated id of each row
    @PrimaryKey(autoGenerate = true)
    private int uid;
    //exclude number
    @ColumnInfo(name = "exclude_number")
    private String excludeNumber;
    //is row checked or not in list
    @ColumnInfo(name = "exclude_checked")
    private int checked;
    //default constructor
    public Exclude(String excludeNumber, int checked) {
        this.excludeNumber = excludeNumber;
        this.checked = checked;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getExcludeNumber() {
        return excludeNumber;
    }

    public void setExcludeNumber(String excludeNumber) {
        this.excludeNumber = excludeNumber;
    }

    public int getChecked() {
        return checked;
    }

    public void setChecked(int checked) {
        this.checked = checked;
    }
}
