package com.twelco.calllog.database.user;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

//dao for entity of user calls
@Dao
public interface UserDao {
    //query to get unsynced users to fill counter
    @Query("SELECT * FROM user WHERE synced = 0")
    List<User> getUnsynced();
    //query to get synced users for last 60 minutes
    @Query("SELECT * FROM user WHERE call_time > :time AND synced=1")
    List<User> getByHour(Long time);
    //query to delete all synced users synced before 00:00
    @Query("DELETE FROM user WHERE call_time < :time AND synced=1")
    void cleanOld(Long time);

    @Insert
    void insertOne(User user);

    @Update
    void update(User users);

}