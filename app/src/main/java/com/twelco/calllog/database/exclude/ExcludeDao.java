package com.twelco.calllog.database.exclude;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

//dao for exlude list entity
@Dao
public interface ExcludeDao {
    //query to get all records from db
    @Query("SELECT * FROM exclude")
    List<Exclude> getAll();

    //query to delete all records
    @Query("DELETE FROM exclude")
    void clearExclude();

    //query to insert one row
    @Insert
    void insertOne(Exclude exclude);
}
