package com.twelco.calllog.service;


import com.twelco.calllog.database.user.User;
//interface to make callback when asynctask finishes work. Result - is the result of work,
// action - description of work that asynctask made and user - entity of User to work with
// it after completing action
interface AsyncComplete {
    void onResponseReceived(Object result, int actionCode, User user);
}