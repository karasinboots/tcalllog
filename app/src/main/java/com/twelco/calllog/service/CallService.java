package com.twelco.calllog.service;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.TelephonyManager;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.RemoteViews;

import com.twelco.calllog.BuildConfig;
import com.twelco.calllog.MainActivity;
import com.twelco.calllog.R;
import com.twelco.calllog.TimerUtil;
import com.twelco.calllog.database.AppDatabase;
import com.twelco.calllog.database.exclude.Exclude;
import com.twelco.calllog.database.user.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import static com.twelco.calllog.ConstantsList.CHANNEL_ID;
import static com.twelco.calllog.ConstantsList.CONFIRM_IN;
import static com.twelco.calllog.ConstantsList.CONFIRM_OUT;
import static com.twelco.calllog.ConstantsList.DEF_IN;
import static com.twelco.calllog.ConstantsList.DEF_OUT;
import static com.twelco.calllog.ConstantsList.ERROR_BROADCAST;
import static com.twelco.calllog.ConstantsList.HANGED;
import static com.twelco.calllog.ConstantsList.INTERNET_BROADCAST;
import static com.twelco.calllog.ConstantsList.NOTIFY_ACCEPT;
import static com.twelco.calllog.ConstantsList.NOTIFY_DECLINE;
import static com.twelco.calllog.ConstantsList.QUERY_BROADCAST;
import static com.twelco.calllog.ConstantsList.REJECTED_CALL;
import static com.twelco.calllog.ConstantsList.STOP_BROADCAST;
import static com.twelco.calllog.ConstantsList.SYNCED_BROADCAST;
import static com.twelco.calllog.ConstantsList.TIMEOUT;
import static com.twelco.calllog.ConstantsList.TRACK_IN;
import static com.twelco.calllog.ConstantsList.TRACK_OUT;
import static com.twelco.calllog.ConstantsList.UPLOAD_BROADCAST;


public class CallService extends Service implements AsyncComplete {


    BroadcastReceiver permissionReceiver;
    BroadcastReceiver uploadReceiver;
    BroadcastReceiver notificationReceiver;
    CallReceiver callReceiver;
    int previousId = 0;
    private SharedPreferences sharedPreferences;
    private Context mContext;
    TimerUtil timerUtil;
    String mIncomingNumber;

    //you use selfsigned certificate, so we need to give permission to HttpsUrlConnection to trust
    //selfsigned certificate. When you sign your certificate you need to delete this.
    final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };

    private static void trustAllHosts() {
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            //overriding certificate checking with empty bodies, so we trust to all
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return new java.security.cert.X509Certificate[]{};
            }

            public void checkClientTrusted(X509Certificate[] chain,
                                           String authType) throws CertificateException {
            }

            public void checkServerTrusted(X509Certificate[] chain,
                                           String authType) throws CertificateException {
            }
        }};
        try {
            //configuring SSLContext to trust all certificates
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            //linking to HttpsUrlConnection configured context
            HttpsURLConnection
                    .setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //this method executed every time we starting service
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //when we return START_NOT_STICKY, so we can stop it
        return START_NOT_STICKY;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        //assign to mContext field of class context of service. it's easier and lighter than
        //make getContext() in every cae
        mContext = getApplicationContext();
        //make notification about running service
        makeNotification("Service is running");
        //creating new instance of TimerUtil and assign it to class field to cancel it when service stopped
        timerUtil = new TimerUtil();
        //get instance of shared preferences(it uses to store simple "name:value" data)
        sharedPreferences = getSharedPreferences("network", MODE_PRIVATE);
        //setting up our broadcast receivers
        setupCallReceiver();
        setupPermissionReceiver();
        setupUploadReceiver();
        setupNotificationReceiver();
        //check query in database with delay in one second
        checkWithDelay();
    }

    private void checkWithDelay() {
        //starting new thread to make delay in 1 second
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //call check(internet permissions) and upload method
                checkAndUpload();
                Log.e("checked with delay", "checked");
            }
        }, 1000);
    }

    private void setupPermissionReceiver() {
        //creating broadcast receiver that track cahnging of network permissions(wifi or wifi+gprs).
        // When it receive broadcast they try to check and upload query
        permissionReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.e("broadcast_service ", "network permission changed");
                checkAndUpload();
            }
        };
        //registering broadcast receiver in system with intent filter IntentFilter(INTERNET_BROADCAST),
        // so he can receive only messages with flag INTERNET_BROADCAST
        LocalBroadcastManager.getInstance(this).registerReceiver(permissionReceiver, new IntentFilter(INTERNET_BROADCAST));
    }

    private void setupCallReceiver() {
        //adding few intent filters to broadcast, so we can receive changing of phone state,
        // registering outgoing calls and network (wif,gprs) changing
        IntentFilter filter = new IntentFilter();
        filter.addAction(TelephonyManager.ACTION_PHONE_STATE_CHANGED);
        filter.addAction(Intent.ACTION_NEW_OUTGOING_CALL);
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        //make new instance of CallReceiver
        callReceiver = new CallReceiver();
        //registering it with intent filters
        mContext.registerReceiver(callReceiver, filter);
    }

    private void setupUploadReceiver() {
        //uploadReceiver receives action when we touch sync buttons and upload data without
        // checking internet permissions
        uploadReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.e("broadcast_service ", "upload");
                getUnsynced();
            }
        };
        //registering receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(uploadReceiver, new IntentFilter(UPLOAD_BROADCAST));
    }

    private void makeNotification(String message) {
        //getting size of unsynced query
        int quantity = AppDatabase.getInstance(mContext).userDao().getUnsynced().size();
        //if query>0 generating message to notification
        if (quantity > 0)
            message = message + " " + "(" + quantity + ")";
        //creating new intent with needed flags to prevent opening more thn one instances of app
        Intent notificationIntent = new Intent(mContext, MainActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        //creating pending intent to link action to notification
        PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        //we need to use different builders of notification to api<26 and >25
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //setting importance of notification(we use default, so it will be in status bar but not heads up)
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            //name of notification
            CharSequence name = "TCallLog";
            //in api>25 we need to create channel of notification, so we can disable
            // notifications with different channels in setting
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            //build notification with title - appname, generated message, channel
            //setOnlyAlertOnce(true) - vibration and sound will be only in first notification,
            //so it will be no sound and vibrate when we updating query counter
            //but if we stop/start service sound will be payed, because it will be new instance of notification
            Notification.Builder builder = new Notification.Builder(mContext, CHANNEL_ID)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText(message)
                    .setChannelId(CHANNEL_ID)
                    .setOnlyAlertOnce(true)
                    .setVibrate(new long[]{0L})
                    .setContentIntent(pendingIntent)
                    .setSmallIcon(R.drawable.ic_active);
            Notification notification = builder.build();
            NotificationManager notificationManager =
                    (NotificationManager) mContext.getSystemService(NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(mChannel);
            //notify with id unique to start/stop notifications, so they cancel each other
            notificationManager.notify(1000, notification);
            //start foreground says system that service running with this notification
            //in api >25 it is necessary
            startForeground(1000, notification);
        } else {
            //notification builder for api < 25, its almost like previous so explanation not needed
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText(message)
                    .setVibrate(new long[]{0L})
                    .setOnlyAlertOnce(true)
                    .setContentIntent(pendingIntent)
                    .setSmallIcon(R.drawable.ic_active)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);
            Notification notification = builder.build();
            NotificationManagerCompat notificationManager =
                    NotificationManagerCompat.from(mContext);
            notificationManager.notify(1000, notification);
            startForeground(1000, notification);
        }
    }

    //notification with timer
    private void makeSelectorNotification(String message, final User user, final boolean outgoing) {
        //we create unique notification to every call, so we set notify id like callLength in milliseconds
        //and then use this to identify notification
        final int notifyId = (int) user.getCallLength();
        //timeout - get timeout from sharedpreferences(we set it in main fragment)
        final int timeout = sharedPreferences.getInt(TIMEOUT, 5000) / 1000;
        //intent for yes button
        final Intent selectorYesIntent = new Intent(mContext, NotificationReceiver.class);
        selectorYesIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        //set broadcast action
        selectorYesIntent.setAction(NOTIFY_ACCEPT);
        //put user entity to intent to write it in database when we receive broadcast with user
        selectorYesIntent.putExtra("user", user);
        //vreating pending intent whit unique id(callLength, it must be match to notifyId)  and linking intent to it.
        PendingIntent piYes = PendingIntent.getBroadcast(mContext, (int) user.getCallLength(), selectorYesIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        //intent for no button
        final Intent selectorNoIntent = new Intent(mContext, NotificationReceiver.class);
        selectorNoIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        selectorNoIntent.setAction(NOTIFY_DECLINE);
        selectorNoIntent.putExtra("user", user);
        PendingIntent piNo = PendingIntent.getBroadcast(mContext, (int) user.getCallLength(), selectorNoIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        //creating remoteview to make custom notification with buttons
        //for remoteview we create layout and need to fill fields on it
        final RemoteViews remoteViews = new RemoteViews(getPackageName(), R.layout.notify_layout);
        //setting message. we pass it when calling this method(makeSelectorNotification)
        remoteViews.setTextViewText(R.id.message, message);
        //setting logo
        remoteViews.setImageViewResource(R.id.logo, R.drawable.logo);
        //linking created earlier pendingintents to buttons
        remoteViews.setOnClickPendingIntent(R.id.btnYes, piYes);
        remoteViews.setOnClickPendingIntent(R.id.btnNo, piNo);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //creating notification with high importance, so we see it like heads up notification
            int importance = NotificationManager.IMPORTANCE_HIGH;
            CharSequence name = "TCallLog";
            final NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID + 1, name, importance);
            final Notification.Builder builder = new Notification.Builder(mContext, CHANNEL_ID + 1)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText(message)
                    .setCustomContentView(remoteViews)
                    .setOngoing(true)
                    .setChannelId(CHANNEL_ID + 1)
                    .setOnlyAlertOnce(true)
                    .setSmallIcon(R.drawable.ic_active);
            Notification notification = builder.build();
            final NotificationManager notificationManager =
                    (NotificationManager) mContext.getSystemService(NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(mChannel);
            notificationManager.notify(notifyId, notification);
            //creating timertask to update timer on notification button every second
            class MyTimerTask extends TimerTask {
                private int i = timeout;

                @Override
                public void run() {
                    if (i > 0) {
                        Log.e("timertask", "execute");
                        //changing buttons on remoteview according to settings in main fragment
                        if (outgoing && sharedPreferences.getBoolean(DEF_OUT, true))
                            remoteViews.setTextViewText(R.id.btnYes, "YES\n" + i);
                        else if (outgoing && !sharedPreferences.getBoolean(DEF_OUT, true))
                            remoteViews.setTextViewText(R.id.btnNo, "NO\n" + i);
                        else if (!outgoing && sharedPreferences.getBoolean(DEF_IN, true))
                            remoteViews.setTextViewText(R.id.btnYes, "YES\n" + i);
                        else if (!outgoing && !sharedPreferences.getBoolean(DEF_IN, true))
                            remoteViews.setTextViewText(R.id.btnNo, "NO\n" + i);
                        //updating builder of notification
                        builder.setCustomContentView(remoteViews);
                        //make updated notification
                        notificationManager.notify(notifyId, builder.build());
                        //when timeout is over make action according to settings in main fragment
                    } else if ((outgoing && sharedPreferences.getBoolean(DEF_OUT, true))
                            || (!outgoing && sharedPreferences.getBoolean(DEF_IN, true))) {
                        //if we select defaultYes - send broadcast with buttonYes intent
                        LocalBroadcastManager.getInstance(mContext).sendBroadcast(selectorYesIntent);
                        //and cancelling timer
                        cancel();
                        return;
                    } else {
                        //if we select defaultYes - send broadcast with buttonNo intent
                        LocalBroadcastManager.getInstance(mContext).sendBroadcast(selectorNoIntent);
                        //and cancelling timer
                        cancel();
                        return;
                    }
                    i--;
                }
            }
            MyTimerTask timerTask = new MyTimerTask();
            Timer timer = new Timer();
            Random randNumber = new Random();
            //scheduling timer with rate 1 sec and random delay up to 100ms
            timer.scheduleAtFixedRate(timerTask, randNumber.nextInt(100), 1000);
            //adding timer and hid unique id to list of timers to clean all if we stop service before notifications gone
            timerUtil.addTimer(new TimerUtil(timer, timerTask, (int) user.getCallLength()));
        } else {
            //everything like before, just for api<26
            final NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID + 1)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText(message)
                    .setCustomContentView(remoteViews)
                    .setOngoing(true)
                    .setOnlyAlertOnce(true)
                    .setVibrate(new long[]{0})
                    .setSmallIcon(R.drawable.ic_active)
                    .setPriority(NotificationCompat.PRIORITY_HIGH);
            Notification notification = builder.build();
            final NotificationManagerCompat notificationManager =
                    NotificationManagerCompat.from(this);
            notificationManager.notify(notifyId, notification);

            class MyTimerTask extends TimerTask {
                private int i = timeout;

                @Override
                public void run() {
                    if (i > 0) {
                        if (outgoing && sharedPreferences.getBoolean(DEF_OUT, true))
                            remoteViews.setTextViewText(R.id.btnYes, "YES\n" + i);
                        else if (outgoing && !sharedPreferences.getBoolean(DEF_OUT, true))
                            remoteViews.setTextViewText(R.id.btnNo, "NO\n" + i);
                        else if (!outgoing && sharedPreferences.getBoolean(DEF_IN, true))
                            remoteViews.setTextViewText(R.id.btnYes, "YES\n" + i);
                        else if (!outgoing && !sharedPreferences.getBoolean(DEF_IN, true))
                            remoteViews.setTextViewText(R.id.btnNo, "NO\n" + i);
                        builder.setCustomContentView(remoteViews);
                        notificationManager.notify(notifyId, builder.build());
                    } else if ((outgoing && sharedPreferences.getBoolean(DEF_OUT, true))
                            || (!outgoing && sharedPreferences.getBoolean(DEF_IN, true))) {
                        LocalBroadcastManager.getInstance(mContext).sendBroadcast(selectorYesIntent);
                        cancel();
                        return;
                    } else {
                        LocalBroadcastManager.getInstance(mContext).sendBroadcast(selectorNoIntent);
                        cancel();
                        return;
                    }
                    i--;
                }
            }
            Timer timer = new Timer();
            MyTimerTask timerTask = new MyTimerTask();
            Random randNumber = new Random();
            timer.scheduleAtFixedRate(timerTask, randNumber.nextInt(100), 1000);
            timerUtil.addTimer(new TimerUtil(timer, timerTask, (int) user.getCallLength()));
        }
    }

    private void setupNotificationReceiver() {
        //setting up notification receiver
        notificationReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //getting user from intent when we receive it
                User user = (User) intent.getExtras().getSerializable("user");
                String action = intent.getAction();
                //to avoid situation when notify on screen and user clicked on
                // button many times, we check previous click id
                if (previousId != (int) user.getCallLength())
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        //getting instance of notification manager to control notification
                        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
                        //check action of broadcast if we touch buttonYes than NOTIFY_ACCEPT, if no - NOTIFY_DECLINE
                        if (NOTIFY_ACCEPT.equals(action)) {
                            //canceling notification
                            notificationManager.cancel((int) user.getCallLength());
                            System.out.println("Service broadcast Alarm Accepted");
                            //so we touch yes and put user to database
                            putUser(user);
                            //finding in list of timers this timer and cancel it
                            for (TimerUtil timerUtil : timerUtil.getList()) {
                                Log.e("timerUtilName", timerUtil.getNumber() + "");
                                if (timerUtil.getNumber() == (int) user.getCallLength()) {
                                    timerUtil.getTimerTask().cancel();
                                }
                            }
                        } else if (NOTIFY_DECLINE.equals(action)) {
                            //if button no - just cancel timer and notification
                            System.out.println("Service broadcast Alarm Declined");
                            notificationManager.cancel((int) user.getCallLength());
                            for (TimerUtil timerUtil : timerUtil.getList()) {
                                if (timerUtil.getNumber() == (int) user.getCallLength()) {
                                    timerUtil.getTimerTask().cancel();
                                }
                            }
                        }
                    } else {
                        //everything like before, just for api<26
                        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(mContext);
                        if (NOTIFY_ACCEPT.equals(action)) {
                            notificationManager.cancel((int) user.getCallLength());
                            System.out.println("Service broadcast Alarm Accepted");
                            putUser(user);
                            for (TimerUtil timerUtil : timerUtil.getList()) {
                                Log.e("timerUtilName", timerUtil.getNumber() + "");
                                if (timerUtil.getNumber() == (int) user.getCallLength()) {
                                    timerUtil.getTimerTask().cancel();
                                }
                            }
                        } else if (NOTIFY_DECLINE.equals(action)) {
                            System.out.println("Service broadcast Alarm Declined");
                            notificationManager.cancel((int) user.getCallLength());
                            for (TimerUtil timerUtil : timerUtil.getList()) {
                                if (timerUtil.getNumber() == (int) user.getCallLength()) {
                                    timerUtil.getTimerTask().cancel();
                                }
                            }
                        }
                    }
                previousId = (int) user.getCallLength();
            }
        };
        //adding needed filters to intentFilter and registering intent
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(NOTIFY_ACCEPT);
        intentFilter.addAction(NOTIFY_DECLINE);
        LocalBroadcastManager.getInstance(this).
                registerReceiver(notificationReceiver, intentFilter);
    }

    //phone state changing receiver
    public class CallReceiver extends PhoneCallReceiver {

        @Override
        protected void onIncomingCallEnded(Context ctx, String number, long start, long end) {
            //when incoming ended - call check exclude list
            mIncomingNumber = number;
            checkExcludeList(new User(number, start, end - start, HANGED, 0, 0));
        }

        @Override
        protected void onOutgoingCallEnded(Context ctx, String number, long start, long end) {
            //when outgoing ended - call check exclude list
            mIncomingNumber = number;
            checkExcludeList(new User(number, start, end - start, HANGED, 1, 0));
        }

        @Override
        protected void onMissedCall(Context ctx, String number, long start) {
            //if we reject call or missed it - also check exclude list
            mIncomingNumber = number;
            checkExcludeList(new User(number, start, start, REJECTED_CALL, 0, 0));
        }

        @Override
        protected void onInternetStateChanged() {
            //try to upload query when changed state of wifi or gprs
            checkAndUpload();
        }
    }


    private void putUser(final User user) {
        new Thread(new Runnable() {
            public void run() {
                //get instance of database
                AppDatabase.getInstance(mContext).userDao().insertOne(user);
                Log.e("service_put_to_db ", sharedPreferences.getString("number", "empty") + "_number_" + user.getCallNumber() + "_calltime_" + user.getCallTime() + "_calllength_" +
                        user.getCallLength() + "_hanged_" + user.getHanged()
                        + "_outgoing_" + user.getIsOutgoing());
                //send broadcast to main fragment to update query counter on menu
                sendQueryBroadcastMessage();
                //checking internet permissions and trying to upload user and query
                checkAndUpload();
            }
        }
        ).start();
        Handler handler = new Handler();
        //making new thread with 10sec delay to update notification if we have query
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //if we have calls in query - make notification
                if (AppDatabase.getInstance(mContext).userDao().getUnsynced().size() != 0)
                    //checking status of service to prevent notification after service stopped
                    if (isMyServiceRunning(CallService.class))
                        makeNotification("Service is running");
            }
        }, 10000);
    }

    private void getUnsynced() {
        //getting unsynced calls in asynctask(new thread and then make action in main thread when work over)
        @SuppressLint("StaticFieldLeak")
        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected List<User> doInBackground(Object[] objects) {
                Log.e("getting_all_unsynced", "");
                //getting unsynced from database
                return AppDatabase.getInstance(mContext).userDao().getUnsynced();
            }

            @Override
            protected void onPostExecute(Object o) {
                //pass result thru interface with action code 1. we dont need to pass user, so send null
                onResponseReceived(o, 1, null);
            }
        };
        asyncTask.execute();
    }

    private void checkExcludeList(final User user) {
        //checking exclude list in ne asynctask
        @SuppressLint("StaticFieldLeak")
        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected List<Exclude> doInBackground(Object[] objects) {
                //getting all numbers from exclude database
                return AppDatabase.getInstance(mContext).excludeDao().getAll();
            }

            @Override
            protected void onPostExecute(Object o) {
                //pass result thru interface with action code 0. also pass user to check number for exclusion
                onResponseReceived(o, 0, user);
            }
        };
        asyncTask.execute();
    }

    //updating user when it successfully synced
    private void updateSynced(final List<User> list) {
        new Thread(new Runnable() {
            public void run() {
                for (User user : list) {
                    //set synced flag to 1
                    user.setSynced(1);
                    //updating in database
                    AppDatabase.getInstance(mContext).userDao().update(user);
                    Log.e("updating user", String.valueOf(user.getCallTime()));
                    //send query message to update counters
                    sendQueryBroadcastMessage();
                }
            }
        }).start();
        //like earlier make notification with counter after 10secs delay
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (AppDatabase.getInstance(mContext).userDao().getUnsynced().size() == 0)
                    //if service running make notification
                    if (isMyServiceRunning(CallService.class))
                        makeNotification("Service is running");
            }
        }, 10000);

    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        //check is service running
        ActivityManager manager = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        //getting list of all running services
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            //and check if name of our service equals name in list
            //if not equals then service is not running
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }

    //upload unsynced calls or call
    private void uploadData(final List<User> list) {
        //creating json array
        final JSONArray jsonArray = new JSONArray();
        //if we have something in received list we put it to array
        if (list.size() > 0)
            new Thread(new Runnable() {
                public void run() {
                    //for all elements of list
                    for (final User item : list) {
                        //put to json array
                        jsonArray.put(makeJsonString(item.getCallNumber(), item.getCallTime(),
                                item.getCallLength(), item.getHanged(), item.getIsOutgoing()));
                    }
                    Log.e("posting data", jsonArray.toString());
                    //when all putted to array - try to send data to server
                    postData(jsonArray.toString(), list);
                }
            }
            ).start();
    }

    @SuppressLint("StaticFieldLeak")
    private void postData(final String jsonData, final List<User> item) {
        //all network actions must be not in main thread
        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                //url to send
                String urlString = "https://sys.twelco.com/smscall_callback.php";
                //add to query parameter datacall and string with data in uri builder
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("datacall", jsonData);
                URL url;
                //getting query from uri
                String data = builder.build().getEncodedQuery();
                //assigning null to outputstream and urlConnection to prevent leak from previous calls
                OutputStream out = null;
                HttpURLConnection urlConnection = null;
                try {
                    //generating Url from string
                    url = new URL(urlString);
                    //if protocol https
                    if (url.getProtocol().toLowerCase().equals("https")) {
                        //then give permission to trust selfsigned certificates and using httpsurlconnection
                        trustAllHosts();
                        //opening connection with url
                        HttpsURLConnection https = (HttpsURLConnection) url.openConnection();
                        //set hostname verifier to trust all
                        https.setHostnameVerifier(DO_NOT_VERIFY);
                        //assigning to urlconnection https connection
                        urlConnection = https;
                    } else {
                        //if not https, just assign httpurlconnection
                        urlConnection = (HttpURLConnection) url.openConnection();
                    }
                    //set post requestmethod
                    urlConnection.setRequestMethod("POST");
                    //set connecction timeout
                    urlConnection.setConnectTimeout(2000);
                    //getting outputstream from urlconnection
                    out = new BufferedOutputStream(urlConnection.getOutputStream());
                    //and create bufferedwriter to write to it
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));
                    //writing generated data of calls to outputstream of connection
                    writer.write(data);
                    //clean writer
                    writer.flush();
                    //close writer
                    writer.close();
                    //close outputstream
                    out.close();
                    //make connection with host
                    urlConnection.connect();
                    //getting answer from server
                    BufferedReader r = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder total = new StringBuilder();
                    String line;
                    //converting answer from server to string
                    while ((line = r.readLine()) != null) {
                        total.append(line).append('\n');
                    }
                    //disconnecting
                    urlConnection.disconnect();
                    Log.e("code", String.valueOf(urlConnection.getResponseCode()));
                    Log.e("server_response", String.valueOf(total).replaceAll("\\s+", ""));
                    //check results and send codes to onPostExecute
                    if (urlConnection.getResponseCode() != 200) return -1;
                    else
                        return Integer.parseInt(String.valueOf(total).replaceAll("\\s+", ""));
                } catch (Exception e) {
                    //if connection timed out send -2
                    System.out.println(e.getMessage());
                    return -2;
                }
            }

            @Override
            protected void onPostExecute(Object o) {
                //actions when response from server received
                switch ((Integer) o) {
                    case -2:
                        //sending broadcast to change status in info field
                        sendErrorBroadcastMessage("no internet connection");
                        //put message to shared preferences to save it after app closes
                        putMessage("no internet connection");
                        break;
                    case -1:
                        sendErrorBroadcastMessage("remote server is temporarily down");
                        putMessage("remote server is temporarily down");
                        break;
                    case 0:
                    case 3:
                    case 4:
                    case 6:
                    case 7:
                        sendErrorBroadcastMessage("remote internal error");
                        putMessage("remote internal error");
                        break;
                    case 1:
                        //if receive 1 send broadcast to update counters and updating synced in database
                        sendSyncedBroadcastMessage();
                        updateSynced(item);
                        break;
                    case 2:
                        sendErrorBroadcastMessage("user does not exist");
                        //stopping service on critical error
                        stop();
                        break;
                    case 5:
                        sendErrorBroadcastMessage("firewall block rule");
                        putMessage("firewall block rule");
                        stop();
                        break;
                    case 8:
                        sendErrorBroadcastMessage("user disabled (support@twelco.com)");
                        putMessage("user disabled (support@twelco.com)");
                        stop();
                        break;
                    case 9:
                        sendErrorBroadcastMessage("user blocked (support@twelco.com)");
                        putMessage("user blocked (support@twelco.com)");
                        stop();
                        break;
                }
            }
        };
        asyncTask.execute();
    }

    private void putMessage(String message) {
        //put message to shared preferences
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("errorMessage", message);
        editor.apply();
    }

    private void sendErrorBroadcastMessage(String message) {
        //sending error broadcast with message
        Intent intent = new Intent(ERROR_BROADCAST);
        intent.putExtra("message", message);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void sendSyncedBroadcastMessage() {
        //sending synced broadcast message with time when appear last success sync
        Intent intent = new Intent(SYNCED_BROADCAST);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong("synctime", System.currentTimeMillis());
        editor.apply();
        Log.e("broadcast_send ", "sended");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void stop() {
        //stop service
        Intent serviceIntent = new Intent(mContext, CallService.class);
        stopService(serviceIntent);
    }

    private JSONObject makeJsonString(String callNumber, long callTime, long callLength, int hanged, int outgoing) {
        //make json object from received data
        JSONObject jObjectData = new JSONObject();
        try {
            //getting uid and user number from shared preferences(we write it in main fragment)
            jObjectData.put("id", sharedPreferences.getString("uid", "not_set"));
            jObjectData.put("u_n", sharedPreferences.getString("number", "not_set"));
            jObjectData.put("n", callNumber);
            jObjectData.put("t", callTime);
            jObjectData.put("l", callLength);
            jObjectData.put("up", hanged);
            jObjectData.put("out", outgoing);
            //get version name from buildconfig
            jObjectData.put("v", BuildConfig.VERSION_NAME.replaceAll("\\s+", ""));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jObjectData;
    }

    private void sendQueryBroadcastMessage() {
        //send query broadcast to update counters and buttons
        Intent intent = new Intent(QUERY_BROADCAST);
        Log.e("broadcast_send ", "sended");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    @Override
    public void onResponseReceived(Object result, int actionCode, User user) {
        //receiving callback from asynctask interface
        switch (actionCode) {
            case 0:
                //in 0 action code check exclude list
                checkExclude((List<Exclude>) result, user);
                break;
            case 1:
                //in 1 action code uploading data
                uploadData((List<User>) result);
                break;
        }

    }

    private void checkExclude(List<Exclude> result, User user) {
        //getting settings from sharedpreferences(do we need track incoming or outgoing,
        // do we need confirmation or not)
        boolean trackIn = sharedPreferences.getBoolean(TRACK_IN, true);
        boolean trackOut = sharedPreferences.getBoolean(TRACK_OUT, true);
        boolean confirmOut = sharedPreferences.getBoolean(CONFIRM_OUT, false);
        boolean confirmIn = sharedPreferences.getBoolean(CONFIRM_IN, false);
        //set excluded flag to false(if we find matches in exclude list then assign true)
        boolean excluded = false;
        List<Exclude> list = result;
        Log.e("service_check_exclude", sharedPreferences.getString("number", "empty") + "_number_" + user.getCallNumber() + "_calltime_" + user.getCallTime() + "_calllength_" +
                user.getCallLength() + "_hanged_" + user.getHanged()
                + "_outgoing_" + user.getIsOutgoing());
        //if exclude list contains numbers
        if (list.size() > 0)
            //then check for any elements of list
            for (Exclude exclude : list) {
                //if received number contains numbers from exclude list and row in exclude checked
                if (mIncomingNumber.contains(exclude.getExcludeNumber()) && exclude.getChecked() != 0) {
                    Log.e("service_check_exclude", "coincidence with item in list - exclude number");
                    //setting flag true and break cycle
                    excluded = true;
                    break;
                } else {
                    //if not matches or matches but not checked - false
                    Log.e("service_check_exclude", "no coincidence with item in list");
                    excluded = false;
                }
            }
        if (!excluded) {
            //if not excluded and we track incoming and it is not outgoing - put to db and send
            // broadcast to update counters
            if (trackIn && !confirmIn && user.getIsOutgoing() == 0) {
                putUser(user);
                sendQueryBroadcastMessage();
            }
            //if not excluded and we track outgoing and it is outgoing - put to db and send
            // broadcast to update counters
            if (trackOut && !confirmOut && user.getIsOutgoing() == 1) {
                putUser(user);
                sendQueryBroadcastMessage();
            }
            //if not excluded and we track incoming and it is not outgoing and need confirmation
            // - make notification with buttons
            if (trackIn)
                if (confirmIn && user.getIsOutgoing() == 0) {
                    String message = "Do you want to log call from " + user.getCallNumber() + " at " + convertDate(user.getCallTime(), "HH:MM") + "?";
                    makeSelectorNotification(message, user, false);
                }
            //if not excluded and we track outgoing and it is outgoing  and need confirmation
            // - make notification with buttons
            if (trackOut)
                if (confirmOut && user.getIsOutgoing() == 1) {
                    String message = "Do you want to log call to " + user.getCallNumber() + " at " + convertDate(user.getCallTime(), "HH:MM") + "?";
                    makeSelectorNotification(message, user, true);
                }
        }
    }

    public static String convertDate(long dateInMilliseconds, String dateFormat) {
        //converting milliseconds to human date with pattern dateFormat
        return DateFormat.format(dateFormat, dateInMilliseconds).toString();
    }

    private void checkAndUpload() {
        //check permissions and upload
        //if we allof gprs and internet is ok - try to download data
        if (NetworkManager.isGprsAllowed(mContext)) {
            if (NetworkManager.isConnected(mContext)) {
                Log.e("network_condition", "gprs allowed, internet is ok");
                getUnsynced();
            } else {
                Log.e("network_condition", "gprs allowed, no internet");
            }
            //if gprs not allowed and wifi available - try to upload
        } else if (NetworkManager.isWifiAvailable(mContext))
            if (NetworkManager.isConnected(mContext)) {
                Log.e("network_condition", "gprs not allowed, wifi available");
                getUnsynced();
            } else {
                Log.e("network_condition", "gprs not allowed, wifi is off");
            }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //clear all timers when service stopped to prevent unnecessary notifications
        for (TimerUtil timerUtil : timerUtil.getList()) {
            Log.e("cancelled", timerUtil.getNumber() + "");
            timerUtil.getTimerTask().cancel();
            timerUtil.getTimer().cancel();
        }
        timerUtil.getList().clear();
        //cancel all notifications when service stopped
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager notificationManager =
                    (NotificationManager) mContext.getSystemService(NOTIFICATION_SERVICE);
            notificationManager.cancelAll();
        } else {
            NotificationManagerCompat notificationManager =
                    NotificationManagerCompat.from(mContext);
            notificationManager.cancelAll();
        }
        //send broadcast to change notification to service stopped
        sendStopBroadcastMessage();
        Log.e("service destroyed!", "destroyed");
        //unregister all receivers to prevent double actions when service starts again
        mContext.unregisterReceiver(callReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(uploadReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(permissionReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(notificationReceiver);

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void sendStopBroadcastMessage() {
        //sending stop broadcast to make service stopped notification
        Intent intent = new Intent(STOP_BROADCAST);
        Log.e("stop_send ", "sended");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }
}


