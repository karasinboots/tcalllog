package com.twelco.calllog.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.util.Calendar;
import java.util.TimeZone;

public abstract class PhoneCallReceiver extends BroadcastReceiver {

    //The receiver will be recreated whenever android feels like it.  We need a static variable to remember data between instantiations

    private static int lastState = TelephonyManager.CALL_STATE_IDLE;
    private static long callStartTime;
    private static boolean isIncoming;
    private static String savedNumber;  //because the passed incoming is only valid in ringing
    private String previousState = "notWIFI";

    @Override
    public void onReceive(Context context, Intent intent) {

        //We listen to three intents.  The new outgoing call only tells us of an outgoing call.  We use it to get the number.
        if (intent.getAction().equals(Intent.ACTION_NEW_OUTGOING_CALL)) {
            //there is two ways to receive outgoing call number in different phones, trying both
            if (intent.getExtras().getString("android.intent.extra.PHONE_NUMBER") != null) {
                Log.e("numer", intent.getExtras().getString("android.intent.extra.PHONE_NUMBER"));
                savedNumber = intent.getExtras().getString("android.intent.extra.PHONE_NUMBER").replaceAll("\\s+", "");
            } else {
                savedNumber = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER).replaceAll("\\s+", "");
                Log.e("numer", intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER).replaceAll("\\s+", ""));
            }
        } else if (intent.getAction().equals(TelephonyManager.ACTION_PHONE_STATE_CHANGED)) {
            String stateStr = intent.getExtras().getString(TelephonyManager.EXTRA_STATE);
            String number = intent.getExtras().getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
            int state = 0;
            //receiving states and pass it to onCallStateChanged
            if (stateStr.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
                state = TelephonyManager.CALL_STATE_IDLE;
            } else if (stateStr.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
                state = TelephonyManager.CALL_STATE_OFFHOOK;
            } else if (stateStr.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
                state = TelephonyManager.CALL_STATE_RINGING;
            }
            onCallStateChanged(context, state, number, intent);
        }
        //if we receive CONNECTIVITY_ACTION action then we need to check is it for the first time or not,
        //because in different phones when we switch wi-fi it can pass 3 or 4 actions
        if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();
            if (activeNetInfo != null) {
                switch (activeNetInfo.getTypeName()) {
                    //if network info pass wifi for the first time we call onInternetStateChanged
                    case "WIFI":
                        //if laststate other than wifi then we receive it for the first time
                        if (!previousState.equals("WIFI")) {
                            onInternetStateChanged();
                            Log.e("received network", activeNetInfo.getTypeName() + "--0");
                        }
                        //and set previous state to "wifi"
                        previousState = "WIFI";
                        break;
                    case "MOBILE":
                        //if mobile call onInternetStateChanged and set laststate to mobile
                        onInternetStateChanged();
                        previousState = "MOBILE";
                        break;
                }
            }
        }
    }

    //Derived classes should override these to respond to specific events of interest
    protected void onIncomingCallStarted(Context ctx, String number, long start) {
    }

    protected void onOutgoingCallStarted(Context ctx, String number, long start) {
    }

    protected void onIncomingCallEnded(Context ctx, String number, long start, long end) {
    }

    protected void onOutgoingCallEnded(Context ctx, String number, long start, long end) {
    }

    protected void onMissedCall(Context ctx, String number, long start) {
    }

    protected void onInternetStateChanged() {
    }

    //Deals with actual events

    //Incoming call-  goes from IDLE to RINGING when it rings, to OFFHOOK when it's answered, to IDLE when its hung up
    //Outgoing call-  goes from IDLE to OFFHOOK when it dials out, to IDLE when hung up
    public void onCallStateChanged(Context context, int state, String number, Intent intent) {

        if (lastState == state) {
            //No change, debounce extras
            return;
        }
        switch (state) {
            case TelephonyManager.CALL_STATE_RINGING:
                isIncoming = true;
                callStartTime = getTime();
                if (number != null)
                    savedNumber = number;
                onIncomingCallStarted(context, number, callStartTime);
                break;
            case TelephonyManager.CALL_STATE_OFFHOOK:
                //Transition of ringing->offhook are pickups of incoming calls.  Nothing done on them
                if (lastState != TelephonyManager.CALL_STATE_RINGING) {
                    isIncoming = false;
                    callStartTime = getTime();
                    onOutgoingCallStarted(context, savedNumber, callStartTime);
                }
                break;
            case TelephonyManager.CALL_STATE_IDLE:
                //Went to idle-  this is the end of a call.  What type depends on previous state(s)
                if (lastState == TelephonyManager.CALL_STATE_RINGING) {
                    //Ring but no pickup-  a miss
                    onMissedCall(context, savedNumber, callStartTime);
                } else if (isIncoming) {
                    onIncomingCallEnded(context, savedNumber, callStartTime, getTime());
                } else {
                    onOutgoingCallEnded(context, savedNumber, callStartTime, getTime());
                }
                break;
        }
        lastState = state;
    }

    private long getTime() {
        //get time by UTC in milliseconds
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        return cal.getTimeInMillis();
    }
}