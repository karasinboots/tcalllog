package com.twelco.calllog.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.twelco.calllog.database.user.User;

import static com.twelco.calllog.ConstantsList.NOTIFY_ACCEPT;
import static com.twelco.calllog.ConstantsList.NOTIFY_DECLINE;

/**
 * Created by karasinboots on 19.12.2017.
 */

public class NotificationReceiver extends BroadcastReceiver {
    Context mContext;

    @Override
    public void onReceive(Context context, Intent intent) {
        mContext = context;
        //notification receiver. receives pending intents when we touch buttns in notifications
        String action = intent.getAction();
        //getting user from intent
        User user = (User) intent.getSerializableExtra("user");
        //and sending broadcast messages with accept or decline action
        if (NOTIFY_ACCEPT.equals(action)) {
            sendBroadcastMessage(NOTIFY_ACCEPT, user);
        } else if (NOTIFY_DECLINE.equals(action)) {
            sendBroadcastMessage(NOTIFY_DECLINE, user);
        }
    }

    private void sendBroadcastMessage(String filter, User user) {
        //sending broadcast with user and needed intent filter
        Intent intent = new Intent(filter);
        intent.putExtra("user", user);
        Log.e("broadcast_send ", filter);
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
    }
}