package com.twelco.calllog.service;


import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkManager {

    public static boolean isConnected(Context context) {
        //check is we have internet connection
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnected();
    }

    public static boolean isWifiAvailable(Context context) {
        //check if wifi available and connected
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        return mWifi.isConnected();
    }

    public static boolean isGprsAllowed(Context context) {
        //check is we permit gprs usage
        SharedPreferences sPref = context.getSharedPreferences("network", Context.MODE_PRIVATE);
        return sPref.getBoolean("allowGprs", false);
    }

}
